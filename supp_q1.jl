### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ d681de5e-8b8a-498a-8b22-2bdc16da5246
using PlutoUI

# ╔═╡ 79a60cee-0c8f-4254-9437-869dc5142699
function generate_pop(pop_size, pop_max)
	population = []
	for pop_counter in 1:pop_size
		push!(population,
			Dict(
				"id" => pop_counter,
				"trait" => (rand() * pop_max)
				)
		)
	end
	return population
end

# ╔═╡ 1a384b2e-6a07-478d-b291-73ab27e1d5bc
generate_pop(50, 1)

# ╔═╡ 973438c6-26e8-431a-8b0d-2c0cbf51540c
md"### Fitness Function"

# ╔═╡ c5d988d7-70e8-46da-b437-dfb88351b802
function fitness(trait)
	return trait/(sum(trait))
end

# ╔═╡ 6408b5cc-bcad-4c73-a43f-13a092019d30
md"### crossover function"

# ╔═╡ 9f763de0-e03f-11eb-22f2-738c61c7de8e
function crossover(parentA, parentB, crossover_point)
	offspring_partA = parentA[1:crossover_point]
	for genome in offspring_partA
		gene = findfirst(el -> el == genome, parentB)
		splice!(parentB, gene)
	end
	return vcat(offspring_partA, parentB)
end

# ╔═╡ 9bc071d3-9f10-4284-8271-229e96a3fc13
md"### Mutation Function"

# ╔═╡ e41fdaa3-9bb0-4e54-98bc-7fd9e142b3a2
function mutate(mutation_rate, child)

	for s in child
		 value = rand(child)
			if value < mutation_rate
	    random_mutation_point1 = rand(1:length(child))
		random_mutation_point2 = rand(1:length(child))
		child[random_mutation_point1], child[random_mutation_point2] 		= child[random_mutation_point2], child[random_mutation_point1]
		return child
	end
end
end

# ╔═╡ e1c2ef81-609a-4bd7-9fac-45b8d0e29859
function shuffle_chromosome(chromosome)
    for i in 1:size(chromosome)[1]
        random_point = rand(1:5, 1)[1]
        chromosome[i], chromosome[random_point] = chromosome[random_point], chromosome[i]
    end
	return chromosome
end

# ╔═╡ bf8ecddc-9f84-4e08-ba8c-187548ada6d3
md"## Solve: Genetic Algrithm
#### mutation, reproduction"

# ╔═╡ b30b525c-89ba-482b-b0ff-d593a00bfd8d
population = generate_pop(50,1)

# ╔═╡ f883f4d7-d4a6-40de-9552-75ece8e37c61
function cal_fitness(chromosome)
	fitness_score =0
	##chromosome = vcat(1, chromosome, 1)
	for geneID in 1:length(chromosome)-1
		trait = (
			population[chromosome[geneID]]["trait"]
			)
		fitness_score += fitness(trait)
	end
	with_terminal() do
		println("Fitness Score", chromosome, ":", fitness_score)
		return fitness_score
	end
end

# ╔═╡ 631ec9ba-ea19-4723-87c8-1f46adb97836
init_chromosome = [1:length(population);]

# ╔═╡ aba32e2a-495e-4b90-a697-1ae87ed813e6
function chromosome_gen(init_pop_size)
	chromosomes = []
	for pop_counterr in 1: init_pop_size
		chromosome = shuffle_chromosome(copy(init_chromosome))
		push!(chromosomes,
		Dict("chromosome" => chromosome, "fitness" => cal_fitness(chromosome)))
	end
	return chromosomes
end

# ╔═╡ 4355d8a2-3b38-4125-b0fe-4506cd9f06c0
chromosomes = chromosome_gen(50)

# ╔═╡ d1e87cc8-1238-4433-a609-8d0b4556063b
function solve(gen_count, child_count, crossover_point, mutation_prob)
	for generation in 1:gen_count
		
			for children in 1:child_count
				println("generation:", generation, "offspring:", child_count)
				
				parent_x_id = rand(1:size(chromosomes)[1])
				parent_y_id = rand(1:size(chromosomes)[1])
				parent_x = copy(chromosomes[parent_x_id]["chromosome"])
				parent_y = copy(chromosomes[parent_y_id]["chromosome"])
				child = crossover(parent_x, parent_y, crossover_point)
				child = mutate(mutation_prob,child)
				push!(chromosomes, 
					Dict("chromosome"=> child,
						"fitness"=>
				cal_fitness(child)
				)
			)
			end
			sort!(chromosomes, by=x ->x["fitness"], rev=false)
			splice!(chromosomes, 6:size(chromosomes)[1])
		end
	end

				

# ╔═╡ 371ce707-f682-4282-ac8b-a5bdd053fd52
solve(10, 10, 5, 0.05)

# ╔═╡ 76f31ed3-60d2-43d2-82f6-a55b4741346e


# ╔═╡ ac2e6396-c92b-4f2b-9dbd-80da552ef391


# ╔═╡ Cell order:
# ╠═d681de5e-8b8a-498a-8b22-2bdc16da5246
# ╠═79a60cee-0c8f-4254-9437-869dc5142699
# ╠═1a384b2e-6a07-478d-b291-73ab27e1d5bc
# ╠═973438c6-26e8-431a-8b0d-2c0cbf51540c
# ╠═c5d988d7-70e8-46da-b437-dfb88351b802
# ╠═f883f4d7-d4a6-40de-9552-75ece8e37c61
# ╠═6408b5cc-bcad-4c73-a43f-13a092019d30
# ╠═9f763de0-e03f-11eb-22f2-738c61c7de8e
# ╠═9bc071d3-9f10-4284-8271-229e96a3fc13
# ╠═e41fdaa3-9bb0-4e54-98bc-7fd9e142b3a2
# ╠═e1c2ef81-609a-4bd7-9fac-45b8d0e29859
# ╠═aba32e2a-495e-4b90-a697-1ae87ed813e6
# ╠═bf8ecddc-9f84-4e08-ba8c-187548ada6d3
# ╠═d1e87cc8-1238-4433-a609-8d0b4556063b
# ╠═b30b525c-89ba-482b-b0ff-d593a00bfd8d
# ╠═631ec9ba-ea19-4723-87c8-1f46adb97836
# ╠═4355d8a2-3b38-4125-b0fe-4506cd9f06c0
# ╠═371ce707-f682-4282-ac8b-a5bdd053fd52
# ╠═76f31ed3-60d2-43d2-82f6-a55b4741346e
# ╠═ac2e6396-c92b-4f2b-9dbd-80da552ef391
